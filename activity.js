db.course.insertMany([

    {

        "name": "HTML Basics",

        "price": 20000,

        "isActive":true,

        "instructor":"Sir Alvin"

    

    },

       {

        "name": "CSS 101 + Flexbox",

        "price": 21000,

        "isActive":true,

        "instructor":"Sir Alvin"

    

    },

         {

        "name": "Javascript",

        "price": 32000,

        "isActive":true,

        "instructor":"Ma'am Tine"

    

    },

     {

        "name": "Git 101, IDE and CLI",

        "price": 19000,

        "isActive":false,

        "instructor":"Ma'am Tine"

      

    },

     {

        "name": "React.js 101",

        "price": 25000,

        "isActive":true,

        "instructor":"Ma'am Miah"

    

    },

 

 

    

   

    ])

    

    

    

    

    // Find course whose instructor is "Sir Alvin" and is priced greater than or equal to 20000

        // show only its name and price.

   



//used with $or method

db.course.find({$or:[{instructor:{$regex: 'Sir Alvin',$options: '$i'}},{price:{$gte:20000}}]})

// used with $and method

db.course.find({$and:[{instructor:{$regex: 'Sir Alvin',$options: '$i'}},{price:{$gte:20000}}]})



// Find course whose instructor is "Ma'am Tine" and is inactive

        //show only its name and price

// 

// db.users.find({$and:[{firstname:{$regex: 'a', $options: '$i'}},{isAdmin:true}]})

db.course.find({$and:[{instructor:{$regex: "Ma'am Tine",$options: '$i'}},{isActive:true}]})



// find course with letter 'r' in its name and has a price lower than or equal to 25000

///used with $or method

db.course.find({$or:[{name:{$regex: 'r',$options:'$i'}},{price:{lte:25000}}]})

//used with $and method



//update all courses with price less tahn 2100 to inactive

    //all prices less than to 21000,  it must be false ({"isActive":false})

db.course.updateMany({price:{$lt:21000}},{$set:{"isActive":false}})



//db.users.deleteMany({"isAdmin":true})

db.course.deleteMany({price:{$gte:25000}})

    